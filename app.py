import os
import boto3
import logging
import firebase_admin
from dotenv import load_dotenv
from flask import Flask, request
from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi
from firebase_admin import credentials, messaging

load_dotenv()

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

app = Flask(__name__)
client = MongoClient(os.environ.get('NOTIFICATIONSERVICE_DB'), server_api=ServerApi('1'))

@app.route('/health', methods=['GET'])
def health():
    return {'status': 'ok'}

@app.route('/notification/register', methods=['POST'])
def new_user():
    logging.info(request.json)
    users = client['RestoRate'].get_collection('Users')
    body = request.json
    if 'Token' not in body:
        return "Token not provided", 400
    upd = users.find_one_and_update({"Token": body['Token']}, {'$set': body})
    print(upd)
    if upd:
        users.update_one(upd, {'$set': body})
    else:
        users.insert_one(body)
    return 'User created', 200

@app.route('/sendNotification', methods=['POST'])
def send_notification():
    logging.info(request.json)
    users = client['RestoRate'].get_collection('Users')
    body = request.json
    tokens = []
    if 'Users' in body:
        for user in body['Users']:
            logging.info(user)
            for db_user in users.find({'AccountGUID': user}):
                tokens.append(db_user['Token'])
    else:
        for user in users.find({}):
            tokens.append(user['Token'])
    message = messaging.MulticastMessage(
        tokens=tokens,
        notification=messaging.Notification(
            title=body['Message']['Title'],
            body=body['Message']['Body'],
            image=body['Message']['Image']
        )
    )
    messaging.send_multicast(message)        
    return "Notification sent", 200

if __name__ == '__main__':
    try:
        client.admin.command('ping')
        logging.info("Successfully connected to MongoDB")
        s3 = boto3.client('s3', endpoint_url=os.environ.get('AWS_ENDPOINT_URL'), aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'), aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY'))
        s3.download_file(os.environ.get('AWS_BUCKET'),'secrets/credentials.json', './credentials.json')
        firebase_cred = credentials.Certificate('./credentials.json')
        firebase_app = firebase_admin.initialize_app(firebase_cred)
        app.run(host='0.0.0.0', port=os.environ.get("NOTIFICATIONSERVICE_PORT"))
    except Exception as e:
        logging.error(e)
